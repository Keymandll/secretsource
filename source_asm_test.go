/*
Copyright 2023 Zsolt Imre

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package secretsource

import (
	"testing"
	"time"
)

func TestGetAwsRegionFromArn(t *testing.T) {
	// Processing valid ARN
	region, err := getAwsRegion("arn:aws:secretsmanager:eu-west-1:603493154151:secret:kubewatch/handler/test-u66pVt")
	if err != nil {
		t.Errorf("Failed to extract region from ARN: %s", err.Error())
	}
	if region != "eu-west-1" {
		t.Errorf("Invalid region extracted from ARN. Expected: 'eu-west-1', received: %s", region)
	}
}

func TestShouldRefreshSecret(t *testing.T) {
	secretExpires := time.Now()
	if shouldRefreshSecret(secretExpires) == false {
		t.Errorf("Failed to detect that the secret expired.")
	}
	secretExpires = time.Now().Add(time.Hour)
	if shouldRefreshSecret(secretExpires) == true {
		t.Errorf("Requesting refresh on non-expired secret.")
	}
	secretExpires = time.Time{}
	if shouldRefreshSecret(secretExpires) == true {
		t.Errorf("Requesting refresh on non-expiring secret.")
	}
}
