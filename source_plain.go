/*
Copyright 2023 Zsolt Imre

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package secretsource

type PlainSecret struct {
	value []byte
}

func (s *PlainSecret) Init(value []byte) error {
	s.value = value
	return nil
}

func (s *PlainSecret) Close() {
	if len(s.value) == 0 {
		return
	}
	var pos int = 0
	for pos < len(s.value) {
		s.value[pos] = 0
		pos++
	}
}

func (s *PlainSecret) Get() ([]byte, error) {
	return s.value, nil
}

func (s *PlainSecret) Refresh() error {
	return nil
}
