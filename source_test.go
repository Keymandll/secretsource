/*
Copyright 2023 Zsolt Imre

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package secretsource

import (
	"reflect"
	"testing"
)

func TestGetSourceAndSecret(t *testing.T) {
	source, value := getSourceAndSecret("test")
	if source != "plain" {
		t.Errorf("Expected plain text secret source handler, received: %s", source)
	}
	if !reflect.DeepEqual(value, []byte("test")) {
		t.Errorf("Expected 'test' as secret, received: %s", value)
	}

	source, value = getSourceAndSecret("plain,test-2")
	if source != "plain" {
		t.Errorf("Expected plain text secret source handler, received: %s", source)
	}
	if !reflect.DeepEqual(value, []byte("test-2")) {
		t.Errorf("Expected 'test-2' as secret, received: %s", value)
	}

	source, value = getSourceAndSecret("asm,test-3")
	if source != "asm" {
		t.Errorf("Expected Amazon Secret Manager source handler, received: %s", source)
	}
	if !reflect.DeepEqual(value, []byte("test-3")) {
		t.Errorf("Expected 'test-3' as secret, received: %s", value)
	}

	source, value = getSourceAndSecret("asm,my,password")
	if source != "asm" {
		t.Errorf("Expected Amazon Secret Manager source handler, received: %s", source)
	}
	if !reflect.DeepEqual(value, []byte("my,password")) {
		t.Errorf("Expected 'my,password' as secret, received: %s", value)
	}
}

func TestSource(t *testing.T) {
	handler, err := SecretSource("plain,my-test-secret")
	if err != nil {
		t.Errorf("Unexpected error: %s", err.Error())
	}
	secretValue, err := handler.Get()
	if err != nil {
		t.Errorf("Get returned with unexpected error: %s", err.Error())
	}
	if !reflect.DeepEqual(secretValue, []byte("my-test-secret")) {
		t.Errorf("Plain text secret source handler returned unexpected secret value. Expected: 'my-test-secret', received: '%s'", secretValue)
	}
}

/*
func TestSourceAws(t *testing.T) {
	handler := SecretSource("asm,smtp-password")
	secretValue := handler.Get()
	fmt.Printf("Password: %s\n", secretValue)
}
*/
