/*
Copyright 2023 Zsolt Imre

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package secretsource

import (
	"reflect"
	"testing"
)

func TestPlainSecretInit(t *testing.T) {
	s := PlainSecret{}
	secret := []byte("unittest")
	err := s.Init(secret)
	if err != nil {
		t.Errorf("Init returned with unexpected error: %s", err.Error())
	}
}

func TestPlainSecretGet(t *testing.T) {
	s := PlainSecret{}
	secret := []byte("unittest")
	s.Init(secret)
	result, err := s.Get()
	if err != nil {
		t.Errorf("Get returned with unexpected error: %s", err.Error())
	}
	if !reflect.DeepEqual(result, secret) {
		t.Errorf("Get returned with unexpected secret value: %s", result)
	}
}

func TestPlainSecretRefresh(t *testing.T) {
	s := PlainSecret{}
	secret := []byte("unittest")
	s.Init(secret)
	err := s.Refresh()
	if err != nil {
		t.Errorf("Refresh returned with unexpected error: %s", err.Error())
	}
	result, err := s.Get()
	if err != nil {
		t.Errorf("Get returned with unexpected error: %s", err.Error())
	}
	if !reflect.DeepEqual(result, secret) {
		t.Errorf("Get returned with unexpected secret value after refresh: %s", result)
	}
}

func TestPlainSecretClose(t *testing.T) {
	s := PlainSecret{}
	secret := []byte("unittest")
	_ = s.Init(secret)
	s.Close()
	expected := []byte{0, 0, 0, 0, 0, 0, 0, 0}
	result, err := s.Get()
	if err != nil {
		t.Errorf("Get returned with unexpected error: %s", err.Error())
	}
	if !reflect.DeepEqual(result, expected) {
		t.Errorf("Close method did not clear secret from memory.")
	}
}
