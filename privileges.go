/*
Copyright 2023 Zsolt Imre

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package secretsource

import (
	"fmt"
	"os"
	"strconv"
	"syscall"
)

/*
 * These functions are here to enable source handlers to switch to a
 * more privileged user to be able to obtain credentials required to
 * authenticate to the external secrets storage.
 *
 * Check out https://gitlab.com/keymandll/external-secrets-client
 * to see why you would want to do this.
 */

func shouldSwitchUser() bool {
	uid := os.Getenv("SECRET_USER_UID")
	gid := os.Getenv("SECRET_USER_GID")
	if len(uid) == 0 || len(gid) == 0 {
		return false
	}
	return true
}

func DropPrivileges() error {
	if !shouldSwitchUser() {
		return nil
	}
	uid := os.Getenv("SECRET_USER_UID")
	gid := os.Getenv("SECRET_USER_GID")
	userId, err := strconv.Atoi(uid)
	if err != nil {
		return fmt.Errorf("Failed to resolve secret user UID: %s\n", err.Error())
	}
	groupId, err := strconv.Atoi(gid)
	if err != nil {
		return fmt.Errorf("Failed to resolve secret user GID: %s\n", err.Error())
	}
	if syscall.Geteuid() == userId && syscall.Getegid() == groupId {
		return nil
	}
	err = switchUser(userId, groupId)
	if err != nil {
		return err
	}
	return nil
}

func GetPrivileges() error {
	if !shouldSwitchUser() {
		return nil
	}
	if syscall.Geteuid() == syscall.Getuid() && syscall.Getegid() == syscall.Getgid() {
		return nil
	}
	err := switchUser(syscall.Getuid(), syscall.Getgid())
	if err != nil {
		return fmt.Errorf("Failed to obtain privileges: %s", err.Error())
	}
	return nil
}

func switchUser(uid int, gid int) error {
	err := syscall.Setegid(gid)
	if err != nil {
		return fmt.Errorf("Failed to set GID: %s\n", err.Error())
	}
	err = syscall.Seteuid(uid)
	if err != nil {
		return fmt.Errorf("Failed to set UID: %s\n", err.Error())
	}
	return nil
}
