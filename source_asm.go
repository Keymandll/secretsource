/*
Copyright 2023 Zsolt Imre

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package secretsource

import (
	"context"
	"errors"
	"fmt"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/secretsmanager"
)

type AwsSecretManagerSecret struct {
	sync.Mutex
	config  aws.Config
	sm      *secretsmanager.Client
	Arn     string
	Region  string
	value   []byte
	Expires time.Time
}

func (s *AwsSecretManagerSecret) Init(value []byte) error {
	secretIdentifier := string(value)
	region, err := getAwsRegion(secretIdentifier)
	if err != nil {
		return fmt.Errorf("Failed to create AWS Secret Manager Secret instance: %s", err.Error())
	}
	c, err := config.LoadDefaultConfig(context.TODO(), config.WithRegion(s.Region))
	if err != nil {
		e := fmt.Sprintf("Failed to load default configuration for AWS Secret Manager handler: %s", err.Error())
		return errors.New(e)
	}
	s.config = c
	s.sm = secretsmanager.NewFromConfig(s.config)
	s.Arn = secretIdentifier
	s.Region = region
	return nil
}

func (s *AwsSecretManagerSecret) Close() {
	s.Arn = ""
	s.Region = ""
	s.Expires = time.Now()
	if len(s.value) == 0 {
		return
	}
	var pos int = 0
	for pos < len(s.value) {
		s.value[pos] = 0
		pos++
	}
}

func (s *AwsSecretManagerSecret) Get() ([]byte, error) {
	if len(s.value) == 0 || shouldRefreshSecret(s.Expires) {
		err := s.Refresh()
		if err != nil {
			return s.value, fmt.Errorf("Failed to refresh secret: %s", err.Error())
		}
	}
	return s.value, nil
}

func awsHandlerDropPrivileges() {
	err := DropPrivileges()
	if err != nil {
		panic("Failed to drop privileges after AWS SM operation.")
	}
}

func (s *AwsSecretManagerSecret) Refresh() error {
	s.Lock()
	defer s.Unlock()
	err := GetPrivileges()
	if err != nil {
		return err
	}
	defer awsHandlerDropPrivileges()
	secretValue, err := awsGetSecretValue(s.sm, s.Arn)
	if err != nil {
		return err
	}
	s.value = secretValue
	expires, expireDate, err := awsGetSecretRotationDate(s.sm, s.Arn)
	if err != nil {
		e := fmt.Sprintf("Failed to check AWS Secret Manager secret expiration date: %s", err.Error())
		return errors.New(e)
	}
	if expires == true {
		s.Expires = expireDate
	}
	return nil
}

func isSecretArn(secretArn string) bool {
	if len(secretArn) > 8 && secretArn[0:8] == "arn:aws:" && len(strings.Split(secretArn, ":")) >= 7 {
		return true
	}
	return false
}

func getAwsRegion(secretArn string) (string, error) {
	if isSecretArn(secretArn) {
		parts := strings.Split(secretArn, ":")
		return parts[3], nil
	}
	return os.Getenv("AWS_REGION"), nil
}

func shouldRefreshSecret(secretExpires time.Time) bool {
	if secretExpires.IsZero() {
		return false
	}
	if secretExpires.Equal(time.Now()) || secretExpires.Before(time.Now()) {
		return true
	}
	return false
}

func awsGetSecretValue(svc *secretsmanager.Client, secretArn string) ([]byte, error) {
	input := &secretsmanager.GetSecretValueInput{
		SecretId:     aws.String(secretArn),
		VersionStage: aws.String("AWSCURRENT"), // VersionStage defaults to AWSCURRENT if unspecified
	}
	result, err := svc.GetSecretValue(context.TODO(), input)
	if err != nil {
		e := fmt.Sprintf("Failed to fetch secret '%s' from AWS Secret Manager: %s", secretArn, err.Error())
		return []byte{}, errors.New(e)
	}
	return result.SecretBinary, nil
}

func awsGetSecretRotationDate(svc *secretsmanager.Client, secretArn string) (bool, time.Time, error) {
	describeSecretIn := &secretsmanager.DescribeSecretInput{
		SecretId: aws.String(secretArn),
	}
	description, err := svc.DescribeSecret(context.TODO(), describeSecretIn)
	if err != nil {
		return false, time.Now(), err
	}
	nextRotationDate := description.NextRotationDate
	if nextRotationDate == nil {
		return false, time.Now(), nil
	}
	return true, *description.NextRotationDate, nil
}
